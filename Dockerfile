# slcli command on softlayer
# version 4.0

# Pull base images.
FROM dockerfile/ubuntu

# Install Python.
RUN apt-get update 
RUN apt-get -y upgrade

# Install pip
RUN apt-get install -y python-pip

# Install softlayer
RUN  pip install softlayer

# Define working directory.
WORKDIR /data

# Define default command.
ENTRYPOINT ["slcli"]

